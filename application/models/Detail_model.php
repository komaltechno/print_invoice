<?php

class Detail_model extends CI_Model
{

    public $keterangan;
    public $rate;
    public $qty;
    public $total;
    private $tableName;

    public function selectAll()
    {
        $query = $this->db->get('detail');
        return $query->result();
    }

    public function findOne($id)
    {
        $this->db->select("*");
        $this->db->from("detail");
        $this->db->where("kode", $id);
        $this->db->limit(1);

        $query = $this->db->get();
        return $query->result()[0];
    }

    public function findAll($where)
    {
        $this->db->select("*");
        $this->db->from("detail");
        $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }

    public function tableName()
    {
        return $this->tableName;
    }

    public function insertDetail($kode, $ket, $rate, $qty, $total)
    {
        $this->kode = $kode;
        $this->keterangan = $ket;
        $this->rate = $rate;
        $this->qty = $qty;
        $this->total = $total;

        $this->db->insert('detail', $this);
        // 	// $data = array(
        // 	// 	'keterangan' => $this->input->post('keterangan'),
        // 	// 	'rate' => $this->input->post('rate'),
        // 	// 	'qty' => $this->input->post('qty'),
        // 	// 	'total' => $this->input->post('total')
        // 	// 	);
        // 	// $insert	= $this->db->insert('detail',$data);
        // 	// return $insert;
        // 	// $keterangan = $this->input->post('keterangan');
        // 	// $rate = $this->input->post('rate');
        // 	// $qty = $this->input->post('qty');
        // 	// $total = $this->input->post('total');
        // 	// $this->db->query("INSERT INTO detail VALUES('$keterangan','$rate','$qty','$total')");
    }

}
