<?php

class Transaksi_model extends CI_Model
{

    public function selectAll()
    {
        $query = $this->db->get('transaksi');
        return $query->result();
    }

    public function findOne($id)
    {
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->where("kode", $id);
        $this->db->limit(1);

        $query = $this->db->get();
        return $query->result()[0];
    }

    public function findTwo($id, $name)
    {
        $a = ['kode' => $id, 'customer' => $name];
        $this->db->select("*");
        $this->db->from('transaksi');
        $this->db->where($a);

        $query = $this->db->get();
        return $query->result();
    }

    public function findAll($where = array())
    {
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }

    public function insertTransaction()
    {
        $this->kode = (!empty($_POST['Input']['kode'])) ? $_POST['Input']['kode'] : null;
        $this->destination = (!empty($_POST['Input']['destination'])) ? $_POST['Input']['destination'] : null;
        $this->tgl = (!empty($_POST['Input']['tgl'])) ? $_POST['Input']['tgl'] : null;
        $this->customer = (!empty($_POST['Input']['customer_transaksi'])) ? $_POST['Input']['customer_transaksi'] : null;
        $this->total = (!empty($_POST['Input']['total'])) ? $_POST['Input']['total'] : null;
        $this->updatedAt = date("Y-m-d H:i:s");

        $this->db->insert('transaksi', $this);
    }

}
