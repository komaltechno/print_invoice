<?php
//namespace application\models;

//use CI_Model;

class Customer_model extends CI_Model
{
    /**
     *
     * @var string
     */
    private $tableName;
    
    
    public function selectAll()
    {
        $query = $this->db->get('customer');
        return $query->result();
    }
    
    public function findOne($id)
    {
        $this->db->select("*");
        $this->db->from("customer");
        $this->db->where("kode",$id);
        $this->db->limit(1);
        
        $query = $this->db->get();
        return $query->result()[0];
    }
    
    public function findAll($where)
    {
        $this->db->select("*");
        $this->db->from("customer");
        $this->db->where("kode",$where);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    public function findByAttributes()
    {
        return $this->tableName;
    }

    public function insertCustomer()
    {
        $this->kode = (!empty($_POST['Input']['kode']))? $_POST['Input']['kode'] : null;
        $this->nama = (!empty($_POST['Input']['nama']))? $_POST['Input']['nama'] : null;
        $this->kota = (!empty($_POST['Input']['kota']))? $_POST['Input']['kota'] : null;

        $this->db->insert('customer',$this);
    }

}
