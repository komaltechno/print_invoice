<?php
include(__DIR__ . '/../layout/head.php');
// echo validation_errors();
?>
<div id="page-wrapper" style="padding-top:20px;">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-list"></i>&nbsp; Form Input Customer
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo form_open('public/Report/inputCustomer'); ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="to">Inisial Customer</label>
                                    <input type="text" name="Input[kode]" size="25" class="form-control"></input>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan">Nama Customer (Lengkap)</label>
                                    <input type="text" name="Input[nama]" size="25" class="form-control" style="text-transform: uppercase;"></input>
                                </div>
                                <div class="form-group">
                                    <label for="rate">Kota</label>
                                    <input type="text" name="Input[kota]" size="25" class="form-control"></input>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="SUBMIT"  name="Input[submit]" class="btn btn-primary pull-right" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $("#kode_customer").on("change", function(){
    var kode_customer = $(this).val();
    $.ajax(
    url: '<?php echo base_url().'Report/getTrans';?>',
    data : {kode : kode_customer},
    success : function(data){
    var transaksi = $("#transaksi");
    }
    );
    });
    </script>
    <?php
    include(__DIR__ . '/../layout/foot.php');
    ?>