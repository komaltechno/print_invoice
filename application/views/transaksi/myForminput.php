<?php
include(__DIR__ . '/../layout/head.php');
// echo validation_errors();
?>
<div id="page-wrapper" style="padding-top:20px;">
    <div class="row">
        <?php echo form_open('public/Report/formInput'); ?>
        <div class="col-lg-6">
            <div class="form-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-list"></i>&nbsp; Form Input Data
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="to">Kode Invoice</label>
                                        <input type="text" name="Input[kode]" size="25" class="form-control"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Pilih Customer</label>
                                        <select id="kode_customer" name="Input[customer]" class="form-control">
                                            <option value="">Pilih Customer</option>
                                            <?php
                                            foreach ($listCustomer as $key => $val) {
                                                echo '<option style="text-transform:uppercase" value="' . $val->kode . '">' . $val->nama . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="total">Tanggal</label>
                                        <input type="date" name="Input[tgl]" size="25" class="form-control"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="to">To</label>
                                        <input type="text" name="Input[to]" size="25" class="form-control"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="to">For</label>
                                        <input type="text" name="Input[for]" size="25" class="form-control"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-list"></i>&nbsp; Detail Transaksi
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="box invoice">
                                <div class="content" style="padding: 0 !important">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="25%">Keterangan</th>
                                                <th width="15%">Rate</th>
                                                <th width="15%">Jumlah</th>
                                                <th style="width:15%">Total</th>
                                                <th width="5%">#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="insertNew">
                                                <td> <input type="text" class="form-control" name="Detail[keterangan][]"/></td>
                                                <td>
                                                    <input class="form-control" prepend="Rp" type="number" value="0" name="Detail[rate][]"/>
                                                </td>
                                                <td><input type="number" class="form-control" name="Detail[qty][]"/></td>
                                                <td>
                                                    <input type="number" class="form-control" prepend="Rp" value="0" name="Detail[total][]"/>
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="hidden" name="detail_id" value="0" class="detail_id">
                                                    <a class="btnAdd btn btn-mini" href="#">[+]</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" valign="middle" align="center"><b>Total</b></td>
                                                <td>
                                                    <div class="input-prepend">
                                                        <span class="add-on">Rp.</span>
                                                        {{data.data}}</div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 center">
            <div class="form-group center">
                <input type="submit" value="SUBMIT"  name="Input[submit]" class="center btn btn-primary center" />
            </div>
        </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#kode_customer").on("change", function () {
        var kode_customer = $(this).val();
        $.ajax({
            url: '<?php echo base_url() . 'Report/getTrans'; ?>',
            data: {kode: kode_customer},
            success: function (data) {
                var transaksi = $("#transaksi");
            }
        });
    });
    $(".btnAdd").bind('click', function () {
        var newLine = "<tr><td><input type='text' class='form-control' name='Detail[keterangan][]'/></td>";
        newLine += '<td><input class="form-control" prepend="Rp" type="number" value="0" name="Detail[rate][]"/></td>';
        newLine += '<td><input type="number" class="form-control" name="Detail[qty][]"/></td>';
        newLine += '<td><input type="number" class="form-control" prepend="Rp" value="0" name="Detail[total][]"/></td>';
        newLine += '<td style="text-align: center"><a class="btnRemove btn btn-mini" href="#">[-]</a></td>';
        newLine += '</tr>';
        $(newLine).insertAfter(".insertNew");
    });
    
    $("body").on('click',".btnRemove",function(){
        $(this).parent().parent().remove();
    });
</script>
<?php
include(__DIR__ . '/../layout/foot.php');
?>