<!DOCTYPE html>
<html>
    <head>
        <title>Print Invoice</title>
        <link rel="stylesheet" href="<?php echo __DIR__ ?>/../../../assets/css/bootstrap.min.css">
    </head>
    <style type="text/css" media="screen">
        h1,h2,h3,h4,h5,h6,p{
            font-weight: 700;
        }
        a{
            text-decoration: none;
        }
        body{
            font-family: "Trebuchet MS","DeJavu Sans";
            font-weight: 500;
            font-size: 1em;
        }
        section{
            min-width: 793.700787402px;
            width:100%;
            padding:5px 25px;
            margin-left:-100px;
        }
        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align:left;
        }
        .p20u{
            padding-top: 20px;
        }
        .grad1 {
            height: auto;
            width:100%;
            background: -webkit-linear-gradient(#C6D4E8, white, #C6D4E8); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#C6D4E8, white, #C6D4E8); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#C6D4E8, white, #C6D4E8); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#C6D4E8, white, #C6D4E8); /* Standard syntax (must be last) */
            margin-right: 10%;
            margin-left: 10%;
            box-shadow: 1px 1px 7px 0;
        }
        .blue{
            color: #3A5D91;
        }
        thead{
            background:#E4EAF4;
            border:2px solid #3A5D91;
        }
        th{
            text-align: center;
            border:1px solid #3A5D91;
            background:#E4EAF4;
        }
        td{
            border:1px solid #3A5D91;
        }
        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 1px solid #3A5D91;
        }
        .no-border{
            border:none;
        }
        .footer{
            margin-top: 50px !important;
        }
        .container{
            width: 100%;
        }

        thead:before, thead:after { display: none; }
        tbody:before, tbody:after { display: none; }
    </style>
    <body>
        <section style="width:80%;margin:0;padding: 0px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="right" style="position:absolute;right:10px;">
                        <h2 class="blue">INVOICE</h2>
                        <br>
                        <br>
                        <br>
                        <p>INVOICE NO : <?php echo $transaction->kode; ?></p>
                        <p>DATE: <?php echo date('d F Y', strtotime($transaction->tgl)); ?></p>

                    </div>
                    <div class="col-md-4 left p20u">
                        <img src="<?php echo __DIR__ ?>/../../../assets/img/logo.png" alt="" height="86" style="margin-left: 37px">
                        <h3><strong>Heasoft Indonesia</strong></h3>
                        <i>House of Enterprise Application</i>
                        <p>&nbsp;</p>
                        <div style="text-align: left">
                            <p>Jl. S. Supriadi 4B No. 630 A Malang</p>
                            <p>Fax 0341-336066 Contact 081 233 288 13</p>
                            <p>Email : <a href="mailto:adjie@heasoft.com" title="" style="text-decoration: none;">adjie@heasoft.com</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        &nbsp;
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-1">
                        <p>TO:</p><br><br><br><br>
                        <div style="text-transform: uppercase;position: fixed;left:5%;top:326.5px;">
                            <p><?php echo $transaction->to; ?></p>
                            <p><?php echo $customer->nama; ?></p>
                            <p><?php echo $customer->kota?></p>
                        </div>

                    </div>
                    <span style="position: fixed;left:50%;top:326.5px;">
                        <p>FOR &nbsp;: <span style="text-transform: capitalize"><?php echo $transaction->for;?></span></p>
                        <p>PO # : n/a</p>
                </div>
            </div>
            <br>
            <div class="container" style="margin-right: 50px !important;">
                <table class="table center">
                    <thead>
                        <tr style="background:#C6D4E8">

                            <th>DESCRIPTION</th>
                            <th>RATE</th>
                            <th>QTY</th>
                            <th>AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $rate = 0;
                        $total = 0;
                        $last = 0;
                        
                        foreach ($detail as $key => $val) {
                            
                            //ngubah format ke Rp
                            $rate = $amountConverter->toRp($val->rate);
                            $total = $amountConverter->toRp($val->total);
                            echo '<tr>';
                            echo '<td><ul>'
                            . '<li>'.$val->keterangan.'</li>'
                            . '</ul></td>';
                            echo '<td>'.$rate.'</td>';
                            echo '<td>'.$val->qty.'</td>';
                            echo '<td>'.$total.'</td>';
                            echo '</tr>';
                            
                            $rate = 0;
                            $total = 0;
                            $last += $val->total;
                        }
                        ?>
                    </tbody>
                    <tbody>
                        <tr class="no-border">
                            <td class="no-border" style="text-align: left">Payment : Cash / Bank Transfer / WU / Others
                                <br>
                                Amount : <i style="font-weight: bold;"><?php echo $amountConverter->terbilang($last)?> Rupiah</i>
                            </td>
                            <td class="no-border">&nbsp;</td>
                            <td class="no-border" style="vertical-align: middle; text-align: right;font-weight: bold;">TOTAL</td>
                            <td style="vertical-align: middle"><?php echo $amountConverter->toRp($last);?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <br>
            <div class="center footer" style="margin-right: 20px !important;">
                <p>BCA Account No. : 8160400316 | BRIS Account No. : 1001 331 082 | Holder Name : Sumariaji</p>
            </div>
        </div>
    </section>
</body>
</html>