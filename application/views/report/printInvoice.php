<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
<!--        <link rel="stylesheet" href="<?php // echo BASEPATH('assets/css/bootstrap.min.css') ?>"/>-->
        <link rel="stylesheet" href="<?php echo base_url('assets/scss/core.scss') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <form method="POST">
            <div class="form-group">
                <label for="foto">provinsi</label>
                <?php
                $dd_provinsi_attribute = 'class="form-control select2"';
//                echo form_dropdown('provinsi', $dd_provinsi, $provinsi_selected, $dd_provinsi_attribute);
                ?>
                <select class="select2" name="customer_list">
                    <?php
                    foreach ($listCustomer as $key => $val) {
                        ?>
                        <option value="<?= $val->kode ?>">
                            <?= $val->nama?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Cari</button> 
        </form>

        <!--jquery dan select2-->
        <!--<script src="<?php // echo BASEPATH('assets/js/jquery-1.12.1.min.js') ?>"></script>-->
        <script src="<?php echo base_url('assets/js/jquery.select2.js') ?>"></script>
        <script>
            $(document).ready(function () {
                $(".select2").select2({
                    placeholder: "Please Select"
                });
            });
        </script>
    </body>
</html>