
<?php
include(__DIR__ . '/../layout/head.php');
// echo validation_errors();
?>

<div id="page-wrapper" style="padding-top:20px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-list"></i>&nbsp; Form Pencarian Data
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-8">
                            <?php echo form_open('public/Report/formPrint'); ?>

                            <div class="form-group col-md-12">
                                <label for="to">Customer</label>
                                <select id="kode_customer" name="Input[kode_customer]" class="form-control">
                                    <option value="">Pilih Customer</option>
                                    <?php
                                    foreach ($listCustomer as $key => $val) {
                                        echo '<option value="' . $val->kode . '">' . $val->nama . "</option>";
                                    }
                                    ?>
                                </select>

                            </div>
                            <div class="form-group col-md-12">
                                <label for="to">Transaksi</label>
                                <select id="transaksi" name="Input[kode_transaksi]" class="form-control">
                                    <option value="">Pilih customer terlebih dahulu</option>
                                </select>   
                            </div>
                            <div class="form-group">
                                <input type="submit" value="SUBMIT"  name="Input[submit]" class="btn btn-primary pull-right" />
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#kode_customer").on("change", function () {
        var kode_customer = $(this).val();
        $.ajax({
            url: '<?php echo base_url('public/Report/getTrans'); ?>',
            type: 'POST',
            data: {
                kode: kode_customer
            },
            success: function (data) {
                $("#transaksi").html(data);
            }
        });
    });

</script>

<?php
include(__DIR__ . '/../layout/foot.php');
?>