<?php

use CI\Framework\System\Core\CI_Controller;

/**
 * Description of Transaksi
 *
 * @author asrulsibaoel
 */
class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->model('Transaksi_model');
        $this->load->model('Detail_model');
    }
    
    public function index()
    {
        $data['transaksi'] = $this->Transaksi_model->selectAll();

        $this->load->view('report/printInvoice');
    }
    
    public function formInput()
    {
        $data = [];

        if ($query = $this->Customer_model->selectAll()) {
            $data['listCustomer'] = $query;
        }

        if (isset($_POST['Input'])) {
            //record transaksi terlebih dahulu;
            $this->Transaksi_model->insertTransaction();
            
            //perulangan sesuai jumlah detailnya;
            for($i = 0; $i < count($_POST['Detail']['keterangan']); $i++){
                $kode = (!empty($_POST['Input']['kode'])) ? $_POST['Input']['kode'] : '';
                $ket = (!empty($_POST['Detail']['keterangan'][$i])) ? $_POST['Detail']['keterangan'][$i] : '';
                $rate = (!empty($_POST['Detail']['rate'][$i])) ? $_POST['Detail']['rate'][$i] : '';
                $qty = (!empty($_POST['Detail']['qty'][$i])) ? $_POST['Detail']['qty'][$i] : '';
                $total = (!empty($_POST['Detail']['total'][$i])) ? $_POST['Detail']['total'][$i] : '';
                
                $this->Detail_model->insertDetail($kode,$ket, $rate, $qty, $total);
            }
            
        }

        $html = $this->load->view('transaksi/myForminput', $data);
    }
}
