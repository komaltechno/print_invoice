<?php

use CI\Framework\System\Core\CI_Controller;


defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->model('Transaksi_model');
        $this->load->model('Detail_model');
    }

    public function index()
    {
        $data['transaksi'] = $this->Transaksi_model->selectAll();

        $this->load->view('report/printInvoice');
    }

    public function pdf($id)
    {
        //ngeload library pdf Generator
        $this->load->library('pdfGenerator');
        $this->load->library('amountConverter');
        $data = array();

        if (!empty($id)) {
            //cari transaksi
            $transaction = $this->Transaksi_model->findOne($id);
            
            //cari customer
            $customer = $this->Customer_model->findOne($transaction->customer);

            //cari detail transaksi
            $detail = $this->Detail_model->findAll(array(
                'kode' => $id
            ));

            if (!empty($detail)) {
                //collect smua ke dalam array;
                $data['transaction'] = $transaction;
                $data['customer'] = $customer;
                $data['detail'] = $detail;
                $data['amountConverter'] = $this->amountconverter;
            }
        }
        $html = $this->load->view('report/printInvoiceResult', $data, true);

        $this->pdfgenerator->generate($html, 'printInvoice', $data);
    }

    public function formPdf()
    {
        /**
         * @var Customer_model $customer
         */
        $this->load->model('Customer_model');
//        log_message('DEBUG',(string);
        $listCustomer = $this->Customer_model->selectAll();
        $html = $this->load->view('report/printInvoice', $listCustomer);
    }

    public function formPrint()
    {
        $data = [];

        if ($query2 = $this->Customer_model->selectAll()) {
            //load data list Customer
            $data['listCustomer'] = $query2;
        }

        //cek $_POST'Input']['kode_transaksi'] kosong/ada apa nggak..
        if (isset($_POST['Input']['kode_transaksi']) && !empty($_POST['Input']['kode_transaksi'])) {
            //kirim ke method pdf($id);
            $this->pdf($_POST['Input']['kode_transaksi']);
        }

        //return ke view
        $this->load->view('report/myFormprint', $data);
    }

    public function getTrans()
    {
        $customer = $_POST['kode'];
        //cari transaksi berdasar customer
        $listTrans = $this->Transaksi_model->findAll(array(
            'customer' => $customer
        ));
        $data = '';
        //bikin option value nya select list Transaksi
        if (!empty($listTrans)) {
            foreach ($listTrans as $key => $val) {
                $data .= '<option value="' . $val->kode . '">' . $val->kode . ' - ' . $val->to . '</value>';
            }
        }
        echo $data;
    }


    public function inputCustomer()
    {
        $data = [];
        $this->load->model('Customer_model');

        if (isset($_POST['Input'])) {
            $data['InputCustomer'] = $this->Customer_model->insertCustomer();
        }

        $html = $this->load->view('customer/inputCustomer', $data);
    }

}
