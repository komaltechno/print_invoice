<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelCore
 *
 * @author asrulsibaoel
 */
abstract class ModelCore extends CI_Model
{
    private $table;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    private static function tableName();
    
    public function findOne($id)
    {
        $this->db->select("*");
        $this->db->from("customer");
        $this->db->where("kode",$id);
        $this->db->limit(1);
        
        return $this->db->get();
    }
    
    public function findAll($where = array(), $limit, $offset)
    {
        $query = $this->db->get_where(self::tableName(), $where, $limit, $offset);
        
        return $query;
    }
    
    public function findByAttributes()
    {
        return $this->tableName;
    }
    
}
